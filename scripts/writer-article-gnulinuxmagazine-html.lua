-- This is a sample custom writer for pandoc.  It produces a content.xml output
-- ready to integrate in a flat OpenOffice document. It follows guidelines
-- defined by Diamond Editions for GNU Linux Magazine France.
--
-- Invoke with: pandoc -t glmf-writer.lua
--
-- Note:  you need not have lua installed on your system to use this custom
-- writer.  However, if you do have lua installed, you can use it to test
-- changes to the script.  'lua sample.lua' will produce informative error
-- messages if your code contains syntax errors.

-- local pipe = pandoc.pipe
-- local stringify = (require "pandoc.utils").stringify

-- The global variable PANDOC_DOCUMENT contains the full AST of
-- the document which is going to be written. It can be used to
-- configure the writer.

require "os"

-- PANDOC_DOCUMENT.meta.prefix est donné par le Makefile. C'est le nom du
-- document généré. Il sert pour forger le nom des images
local meta_prefix = "prefix"
if PANDOC_DOCUMENT.meta.prefix then
  meta_prefix = PANDOC_DOCUMENT.meta.prefix
end

local meta_images = false
if PANDOC_DOCUMENT.meta.images then
  meta_images = string.lower(PANDOC_DOCUMENT.meta.images) == "on"
  if meta_images then
    print("[WARNING] Images are included in the final ODT")
  end
end

local meta_class = "class"
if PANDOC_DOCUMENT.meta.class then
  meta_class = PANDOC_DOCUMENT.meta.class
end

--[[ rPrint(struct, [limit], [indent])   Recursively print arbitrary data.
	Set limit (default 100) to stanch infinite loops.
	Indents tables as [KEY] VALUE, nested tables as [KEY] [KEY]...[KEY] VALUE
	Set indent ("") to prefix each line:    Mytable [KEY] [KEY]...[KEY] VALUE
--]]
-- Credits: https://gist.github.com/stuby/5445834#file-rprint-lua
function rPrint(s, l, i) -- recursive Print (structure, limit, indent)
  l = (l) or 100
  i = i or "" -- default item limit, indent string
  if (l < 1) then
    print "ERROR: Item limit reached."
    return l - 1
  end
  local ts = type(s)
  if (ts ~= "table") then
    print(i, ts, s)
    return l - 1
  end
  print(i, ts) -- print "table"
  for k, v in pairs(s) do -- print "[KEY] VALUE"
    l = rPrint(v, l, i .. "\t[" .. tostring(k) .. "]")
    if (l < 0) then
      break
    end
  end
  return l
end

-- Table to store footnotes, so they can be included at the end.
local notes = {}

-- Tableau qui stoque les numéros de titres (ici jusqu'à 4 niveaux)
local titres = {0, 0, 0, 0}

-- Liste des figures indexées par le caption (identifiant)
local figures = {}

-- Pré-définition de la référence et de la légende de la prochaine image
local next_image = {["caption"] = nil, ["title"] = nil}

-- Flags pour contrôler l'ajout des références automatiques et les chapitres
-- qui suivent la conclusion le cas échéant. ("Pour aller plus loin")
local referencesAdded = false
local conclusionAdded = false

-- Retourne le prochain numéro de titre pour un niveau (de 1 à 4) donné
local function getTitreStr(n, s)
  local str = ""
  if n == 1 and s == "Conclusion" then
    conclusionAdded = true
  end
  if conclusionAdded then
    return ""
  end
  if n < 0 or n > 4 then
    return ""
  end
  titres[n] = titres[n] + 1
  for i = n + 1, 4 do
    titres[i] = 0
  end

  for i = 1, n - 1 do
    str = str .. titres[i] .. "."
  end

  if meta_class == "modele-article-linuxpratique-tutoriel" and n == 1 then
    return "Étape " .. str .. titres[n] .. " : "
  end

  return str .. titres[n] .. " "
end

-- Retourne la caption et le numéro de la figure courante (Cite & CaptionedImage)
function getCaptionNum(caption)
  -- caption est l'identifiant de la figure
  -- num est le numéro de la figure
  if caption == "next" then
    return caption, #figures + 1
  elseif caption == "last" then
    return caption, #figures
  end
  local num = 0
  if caption == "" then
    num = #figures + 1
    caption = "__" .. num .. "__"
  else
    for i, v in pairs(figures) do
      if v == caption then
        num = i
        break
      end
    end
    if num == 0 then
      num = #figures + 1
    end
  end
  figures[num] = caption
  -- print("*** Image", caption, num)
  return caption, num
end

-- Table to store footnotes, so they can be included at the end.
local notes = {}

local function escape(s)
  local xml_special = {["&"] = "&amp;", ["<"] = "&lt;", [">"] = "&gt;", ['"'] = "&quot;"}
  return s:gsub(
    '[&<>"]',
    function(c)
      return xml_special[c]
    end
  )
end

-- Implements a simple cache system based a MD5 digest of the data
local function getCacheFilename(data, ext)
  local ftmp = os.tmpname()
  local md5 = io.popen("md5sum >" .. ftmp, "w")
  md5:write(data)
  md5:close()
  md5 = io.open(ftmp, "r")
  local hdata = md5:read(32)
  md5:close()
  os.remove(ftmp)
  local fname = "cache/" .. hdata .. ext
  local f = io.open(fname, "r")
  local fexists = f ~= nil
  if fexists then
    io.close(f)
  end
  return fexists, fname
end

local function RunSvgTool(cmd, arg, s)
  local exists, cname = getCacheFilename(s, ".svg")
  if not exists then
    local uname = os.tmpname()
    local file = io.open(uname, "w")
    file:write(s)
    file:close()
    print("Running " .. cmd .. "...")
    os.execute(cmd .. " " .. arg .. " <" .. uname .. " >" .. cname)
    os.remove(uname)
  end
  return CaptionedImage(cname, "", "", attr)
end

local function Plantuml(s, type)
  s = "@start" .. type .. "\n" .. s .. "\n@end" .. type .. "\n"
  return RunSvgTool("plantuml", "-tsvg -p", s)
end

local function Dot(s)
  s = s .. "\n"
  return RunSvgTool("dot", "-Tsvg", s)
end

local function addReferences()
  if referencesAdded then
    return ""
  end

  referencesAdded = true

  local buffer = {}

  local function add(s)
    table.insert(buffer, s)
  end

  if #notes > 0 then
    add("<h2>Références</h2>")
    add("<div class=refs>")
    for i, note in pairs(notes) do
      local d = note:find(">")
      note = note:sub(1, d) .. Strong("[" .. i .. "]") .. " " .. note:sub(d + 1)
      add(note)
    end
    add("</div>")
  end

  return table.concat(buffer, "\n") .. "\n"
end

-- Blocksep is used to separate block elements.
function Blocksep()
  return "\n"
end

function Doc(body, metadata, variables)
  return body .. addReferences()
end

function Str(s)
  return escape(s)
end

function Space()
  return " "
end

function SoftBreak()
  return "\n"
end

function LineBreak()
  return "<br/>"
end

function Emph(s)
  return "<em>" .. s .. "</em>"
end

function Strong(s)
  return "<strong>" .. s .. "</strong>"
end

function Subscript(s)
  return "<sub>" .. s .. "</sub>"
end

function Superscript(s)
  return "<sup>" .. s .. "</sup>"
end

function SmallCaps(s)
  return '<span style="font-variant: small-caps;">' .. s .. "</span>"
end

function Strikeout(s)
  return "<del>" .. s .. "</del>"
end

function Link(s, tgt, tit, attr)
  return "<a href='" .. escape(tgt, true) .. "' title='" .. escape(tit, true) .. "'>" .. s .. "</a>"
end

function Image(s, src, tit, attr)
  return "*** Image NOT IMPLEMENTED ***\n"
end

function Code(s, attr)
  return "<code class='code_em'>" .. escape(s) .. "</code>"
end

function InlineMath(s)
  return Plantuml(s, "math")
end

function DisplayMath(s)
  return Plantuml(s, "latex")
end

function SingleQuoted(s)
  return "« " .. s .. " »"
end

function DoubleQuoted(s)
  return "« " .. s .. " »"
end

function Note(s)
  -- Il faut rechercher s, et si on trouve pas, on rajoute un élément dans la
  -- table notes.
  local num = -1
  for i, v in pairs(notes) do
    if v == s then
      num = i
      break
    end
  end

  if num < 0 then
    num = #notes + 1
    table.insert(notes, s)
  end

  return Strong("[" .. num .. "]")
end

function getKeySpan(keys)
  local str = ""
  for key in keys:gmatch("[^ |\t]+") do
    if key == "+" then
      str = str .. " + "
    else
      str = str .. " " .. Strong(escape("<") .. key .. escape(">"))
    end
  end
  return str
end

function Span(s, attr)
  if attr.class == "m" or attr.class == "menu" then
    return '<span class="menu">' .. s .. '</span>'
  elseif attr.class == "k" or attr.class == "keys" then
    return getKeySpan(s)
  end
  return "*** Span NOT IMPLEMENTED ***\n"
end

function RawInline(format, str)
  -- DEPRECATED: Dans GLMF les <xxx> sont des touches clavier.
  -- return Strong(escape(str)).
  return "*** RawInline NOT IMPLEMENTED ***\n"
end

function Cite(c, cs)
  -- caption est l'identifiant de la figure
  -- num est le numéro de la figure
  local num = 0
  next_image.caption, next_image.title = c:match("^%[@figure%s+([%w_-]+)%s+(.*)%]$")

  if next_image.caption == nil then
    -- It's not a define
    caption = c:match("^%[@([%w_-]+)")
    if caption == "" then
      return ""
    end
    caption, num = getCaptionNum(caption)
    return tostring(num)
  else
    -- It's just a define
    caption, num = getCaptionNum(next_image.caption)
    return ""
  end
end

function Plain(s)
  return s
end

function Para(s)
  return "<p>" .. s .. "</p>"
end

-- lev is an integer, the header level.
function Header(lev, s, attr)
  local refs = ""
  if conclusionAdded and s ~= "Remerciements" then
    refs = addReferences()
  end
  return refs .. "<h" .. lev + 1 .. ">" .. getTitreStr(lev, s) .. s .. "</h" .. lev + 1 .. ">"
end

function BlockQuote(s)
  return "<blockquote>\n" .. s .. "\n</blockquote>"
end

function HorizontalRule()
  return "<hr/>"
end

function LineBlock(ls)
  return '<div style="white-space: pre-line;">' .. table.concat(ls, "\n") .. "</div>"
end

function CodeStrong(s)
  return '<span class="code_em">' .. s .. "</span>"
end

function CodeBlockLogo(s)
  local lines = {}
  for line in s:gmatch("([^\n]*)\n?") do
    table.insert(lines, '<text:p text:style-name="pragma">/// Logo : ' .. line .. " ///</text:p>\n")
  end
  return table.concat(lines, "\n")
end

function CodeBlock(s, attr)
  local class = string.lower(attr.class)

  -- rPrint(attr)
  if attr.include then
    s = ""
    local start = attr.start or ""
    local stop = attr.stop or "^\n$"
    local started = false
    for line in io.lines(attr.include) do
      if line:find(start) then
        started = true
      end
      if started then
        s = s .. line .. "\n"
      end
      if started and line:find(stop) then
        break
      end
    end
  end

  -- Pragma Logos
  if class == "logo" or class == "logos" then
    return CodeBlockLogo(s)
  end

  -- Plantuml source
  if class:find("plantuml") then
    return Plantuml(s, "uml")
  end

  -- Dot source
  if class:find("dot") then
    return Dot(s)
  end

  -- Source code or console text
  if class == "" or class:find("text") or class:find("console") then
    class = "text"
  end
  local lines = {"<pre><code class=" .. class .. ">"}

  local numberLines = class:find("numberlines") ~= nil
  local lineNumber = tonumber(attr.startFrom or 1)
  local numberFormat = attr.format or "%2d: "
  local step = tonumber(attr.step or 1)

  -- get code_em* attributes if any
  local code_em_re = {}
  local code_em_keys = {}
  for k, v in pairs(attr) do
    if k:find("^code_em.*$") then
      table.insert(code_em_keys, k)
      code_em_re[k] = "^" .. v
    end
  end
  table.sort(code_em_keys)

  -- for each line in code
  for line in s:gmatch("([^\n]*)\n?") do
    out = ""
    -- handle line number if any
    if numberLines then
      -- line = numberFormat:format(lineNumber) .. line
      out = numberFormat:format(lineNumber)
      lineNumber = lineNumber + step
    end
    -- handle code_em
    while #line > 0 do
      local m = nil
      for _, k in ipairs(code_em_keys) do
        d, f, m = line:find(code_em_re[k])
        if d ~= nil then
          if m == nil then
            m = line:sub(d, f)
          else
            d, f = line:find(m)
            -- print("***", m, escape(line:sub(1, d - 1)), line:sub(d))
            -- Remove chars before match from line and add them to out
            out = out .. escape(line:sub(1, d - 1))
            line = line:sub(d)
          end
        end
        if m then
          break
        end
      end
      if m == nil then
        out = out .. escape(line:sub(1, 1))
        line = line:sub(2)
      else
        -- FIXME: code_em4="_Liste(Tab)" => match renvoie Tab
        -- Trouver le nombre de caractères
        line = line:sub(#m + 1)
        out = out .. CodeStrong(escape(m))
      end
    end
    line = out
    table.insert(lines, line)
    -- TODO: handle syntax highlighting
  end
  table.insert(lines, "</code></pre>")
  return table.concat(lines, "\n")
end

function BulletList(items)
  local buffer = {}
  for _, item in pairs(items) do
    table.insert(buffer, "<li>" .. item .. "</li>")
  end
  return "<ul>\n" .. table.concat(buffer, "\n") .. "\n</ul>"
end

function OrderedList(items)
  local buffer = {}
  for _, item in pairs(items) do
    table.insert(buffer, "<li>" .. item .. "</li>")
  end
  return "<ol>\n" .. table.concat(buffer, "\n") .. "\n</ol>"
end

function DefinitionList(items)
  local buffer = {}
  for _, item in pairs(items) do
    local k, v = next(item)
    table.insert(buffer, "<dt>" .. k .. "</dt>\n<dd>" .. table.concat(v, "</dd>\n<dd>") .. "</dd>")
  end
  return "<dl>\n" .. table.concat(buffer, "\n") .. "\n</dl>"
end

-- Convert pandoc alignment to something HTML can use.
-- align is AlignLeft, AlignRight, AlignCenter, or AlignDefault.
local function html_align(align)
  if align == "AlignLeft" then
    return "left"
  elseif align == "AlignRight" then
    return "right"
  elseif align == "AlignCenter" then
    return "center"
  else
    return "left"
  end
end

-- /// Image : nom_article_figure_01.png ///
function CaptionedImage(src, title, caption, attr)
  local num = 0

  if caption == "" and next_image.caption ~= nil then
    caption = next_image.caption
    if next_image.title ~= nil then
      title = next_image.title
    end
  end
  next_image.caption = nil
  next_image.title = nil

  caption, num = getCaptionNum(caption)

  local buffer = {}
  local function add(s)
    table.insert(buffer, s)
  end

  local ext = string.sub(src, string.find(src, "%.%a+$"))
  local dst = string.format("%s_figure_%02d%s", meta_prefix, num, ext)
  if src:find("//") then
    local exists, cfile = getCacheFilename(src, ext)
    if not exists then
      print("Downloading '" .. src .. "'")
      os.execute("curl " .. src .. " -s -o " .. cfile)
    end
    src = cfile
  end
  print("Copying '" .. src .. "' to 'build/" .. dst .. "'")
  os.execute("cp " .. src .. " build/" .. dst)
  if title:match("^fig:") then
    title = string.sub(title, 5)
  end
  if title ~= "" then
    title = " : " .. title
  end
  if meta_images then
    dim = ""
    local imgmgk = io.open("/usr/bin/identify", "r")
    if imgmgk then
      imgmgk:close()
      local identify = io.popen('/usr/bin/identify -format "%w %h" build/' .. dst, "r")
      local w, h = identify:read("*number", "*number")
      identify:close()
      -- Points par cm
      local ppcm = 64
      -- max 16 cm en largeur
      local w_cm = math.min(12, w / ppcm)
      -- max 8 cm en hauteur
      local h_cm = math.min(8, h / ppcm)
      -- aspect ratio
      if w > h then
        h_cm = w_cm * h / w
      else
        w_cm = h_cm * w / h
      end
      dim = ' width="' .. w_cm * ppcm .. '" height="' .. h_cm * ppcm .. '"'
    end
    add('<div class="figure"><img src="' .. dst .. '"' .. dim .. "/>")
  end
  add("<p><center><i>Fig. " .. num .. title .. "</i></center></p></div><br>")

  return table.concat(buffer, "\n")
end

-- Caption is a string, aligns is an array of strings,
-- widths is an array of floats, headers is an array of
-- strings, rows is an array of arrays of strings.
function Table(caption, aligns, widths, headers, rows)
  local buffer = {}

  local function add(s)
    table.insert(buffer, s)
  end
  add("<center><table>")
  if caption ~= "" then
    add("<caption>" .. caption .. "</caption>")
  end
  if widths and widths[1] ~= 0 then
    for _, w in pairs(widths) do
      add('<col width="' .. string.format("%.0f%%", w * 100) .. '" />')
    end
  end
  local header_row = {}
  local empty_header = true
  for i, h in pairs(headers) do
    local align = html_align(aligns[i])
    table.insert(header_row, '<th align="' .. align .. '">' .. h .. "</th>")
    empty_header = empty_header and h == ""
  end
  if empty_header then
    head = ""
  else
    add('<tr class="header">')
    for _, h in pairs(header_row) do
      add(h)
    end
    add("</tr>")
  end
  local class = "even"
  for _, row in pairs(rows) do
    class = (class == "even" and "odd") or "even"
    add('<tr class="' .. class .. '">')
    for i, c in pairs(row) do
      add('<td align="' .. html_align(aligns[i]) .. '">' .. c .. "</td>")
    end
    add("</tr>")
  end
  add("</table></center>")
  return table.concat(buffer, "\n")
end

function RawBlock(format, str)
  if format == "html" then
    return str
  else
    return ""
  end
end

function Div(s, attr)
  local class = string.lower(attr.class)
  local debut_note = ""
  local fin_note = "note"

  if class == "nda" or class == "ndlr" then
    s = s:gsub("^<.->", '<text:p text:style-name="note">')
    return s
  elseif class == "note" then
    debut_note = "Note"
  elseif class == "attention" then
    debut_note = "Attention"
  elseif class == "avertissement" then
    debut_note = "Avertissement"
  elseif class == "pao" then
    debut_note = "Note PAO"
    fin_note = debut_note
  elseif class == "encadre" or class == "encadré" then
    debut_note = ""
    fin_note = debut_note
    s = s:gsub("^<p>", "<h2>")
    s = s:gsub("</p>", "</h2>")
  else
    debut_note = ""
    fin_note = debut_note
  end
  if debut_note ~= "" then
    return "<div><h2>" .. debut_note .. "</h2>\n" .. s .. "</div>\n"
  else
    return "<div>" .. s .. "</div>\n"
  end
end
